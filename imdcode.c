/**
 ** =========================================================================================
 ** SiMS IMD Security Protocol
 ** Scenario: <IMD wakes up through RF link and mutually authenticates wtih external reader>
 ** =========================================================================================
 **
 ** Nomenclature:
 ** - {a}_K	: CMAC code of message 'a' using key 'K'
 ** - |a|_K	: Ciphertext of (plaintext) message 'a' using key 'K'
 **
 ** Communication packet sizes:
 ** - Incoming packet size: 192 bits (= 24 Bytes = 6 u4 words)
 ** - Outgoing packet size: n*192 bits (= n*24 Bytes = n*6 u4 words), n: positive integer
 ** =========================================================================================
 **
 ** To-Do List (high to low priority):
 ** - (!) Fix weird bug manifesting when setting ANSi_LEN >= 64 (proper max size: 576) [Could be a VirtualBox error]
 ** - Provide different initial factory seeds for Tij and Trj numbers (64b)
 ** - Why is there no IDr included in the MAC the IMPLANT sends to the READER?
 ** - Specify READER command (CMD) fields (32b)
 ** - Big-/Little-Endian considerations???
 ** - Transceiver payload size??
 **
 ** =========================================================================================
 **
 ** SiMS/SISC C-compiler unsupported features:
 **	(1) Shifting by a registered value. E.g. int a = 4; int b = 3; int c = a << b;
 **	(2) Any <*.h> include (and, accordingly, function call);
 **	(3) Any floating point (single / double) arithmetic / conversion or return value;
 **	(4) Any long long arithmetic / conversion or return value;
 **	(5) Any jump and link to registered value.
 **		E.g. void foo(void){;} void main(){void * a = &foo; a();}
 **
 ** Explanation:
 **	(1) and (5)	: missing instructions / routines
 **	(2)		: 64-bit support needed to compile the libraries
 **	(3) and (4)	: 64-bit support needed / missing routines (floats)
 **	This also means that e.g. "long long" variables (64 bits) are also not supported.
 **
 ** =========================================================================================
 ** Created:		Dimitrios Siskos - 01/01/2010
 ** Last modified:	Christos Strydis - 28/03/2013
 ** =========================================================================================
 *   Copyright (c) 2013, Neurasmus B.V., The Netherlands,
 *   web: www.neurasmus.com email: info@neurasmus.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, Version 2.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


// =================================================
// == Simulation defines & libraries ===============
// =================================================
// NOTE: For accurate performance/power profiling, select (0, 0/1, 0, 0)

#define READEREMU 0 // Enable/Disable the READER activity; when disabled, precalculated, dummy values are used to fool the various checks
#define KEYGEN    0 // Enable/Disable expanded & subkey-creation; when disabled, precalculated values are used (and processing is reduced)
#define PRINTOUTS 0 // Enable/Disable printing information to stdout
#define MSGDUMP   0 // Enable/Disable printing message dumps to stdout (if PRINTOUTS is also enabled)


#include "misty1.h"

#if PRINTOUTS==1
#include <stdio.h>
#include <string.h>
#endif


// =================================================
// == Application defines & typedefs ===============
// =================================================

// Public IDs of IMPLANT and READER
#define IDi_HI 0x000000 // IMD identifier high word (total of 96 bits)
#define IDi_ME 0x000000 // IMD identifier middle word
#define IDi_LO 0x000064 // IMD identifier low word

#define IDr_HI 0x000000 // READER identifier high word (total of 96 bits)
#define IDr_ME 0x000000 // READER identifier middle word
#define IDr_LO 0x0000C8 // READER identifier low word


// Shared block-cipher private key
#define KEY_1 0x00112233
#define KEY_2 0x44556677
#define KEY_3 0x8899AABB
#define KEY_4 0xCCDDEEFF


#define ZERO_LO 0x00000000 // Zero-padded low word
#define ZERO_HI 0x00000000 // Zero-padded high word


// Primitive polynomial for creating block-cipher sub-keys K1 and K2
#define Rb_LO 0x0000001B // CMAC polynomial low word
#define Rb_HI 0x00000000 // CMAC polynomial high word


// Seeds for the PRN-generators (provided by the factory)
#define SEEDi_LO 0x0F0F0F0F
#define SEEDi_HI 0x0E0E0E0E

#define SEEDr_LO 0x1F1F1F1F
#define SEEDr_HI 0x1E1E1E1E


// (Max) Size of command and answer fields (in u4 multiples)
#define CMDr_LEN 1
#define ANSi_LEN 32 //FIXME (576) // The >maximum< IMD answer size (maxed out for transmitting 1 day of logged Biostator data: 24 hours * 12 samples/hour * 4 u4/sample = 576 u4's are needed)


// Size of >input< arguments to the CMAC algorithm (in u4 multiples)
#define MACr_INLEN (1 + 1 + 3 + CMDr_LEN) // The length of the input vector fed to the CMAC algorithm on the READER side
#define MACi_INLEN (1 + 1 + ANSi_LEN) // The >maximum< length of the input vector fed to the CMAC algorithm on the IMD side


// Size of wirelessly transmitted packets (in u4 multiples)
#define RF_IN_LEN (1 + 1 + 2 + (CMDr_LEN+1)) // The length of the IMPLANT incoming messages (the CMD field is zero-padded to 64 bits)
#define RF_OUT_LEN (2 + ANSi_LEN) // The >maximum< length of the IMPLANT outgoing messages



int noskip_flag = 1; // used to bypass all checks when READEREMU = 0


#if MSGDUMP==1
// Debug function that prints the sent and received messages
void printm(u4 msg[], int len) {
	int i;

	for(i=0; i<len; i++)
			printf("\n- 0x%08X ", msg[i]);
}
#endif


//==================================================
// == CMAC functions ===============================
//==================================================

// Random number generator based on MISTY1 block cipher - Counter mode
// IMPORTANT: T and O must be preallocated as u4 T[2], O[2];
u4 crand(u4 *ek, u4 *T, u4 *O) {
	u4 Rn;

	misty1_encrypt_block(ek, T, O);
	Rn = O[0]; // new random number = O[31..0]
	T[1] = 0x00000000; // T[63..32] = use old T[0] or zero-pad (0x00000000)
	T[0] = O[1]; // T[31..0] = O[63..32]

	return Rn;
}


#if KEYGEN==1
// Function to generate block-cipher subkeys (of size MISTY1_BLOCKSIZE) from expanded key ek
// Note: In this instance, K2 is not used as M_len is a multiple of the MISTY1_BLOCKSIZE; i.e. computations are simplified
// K1 is used when CMAC messages are exact multiples of the MISTY1_BLOCKSIZE;
// K2 is used on the final CMAC chunk when the whole message is not a multiple of the MISTY1_BLOCKSIZE.
// IMPORTANT: K1, K2 must be preallocated as u4 K1[2], K2[2];
void subkeys(u4 *ek, u4 *K1/*, u4 *K2*/) {
	u4 L[2] = {ZERO_LO, ZERO_HI};

	// Step 1
	misty1_encrypt_block(ek, L, L);

	// Step 2: K1 generation
	if ( (L[1] & 0x40000000) == 0x00000000 ) {
		K1[1] = ( (L[0] & 0x40000000) == 0x40000000 ) ? ( L[1]<<1 | 0x00000001 ) : ( L[1]<<1 ); // shift left and add 1 carry to K1[1] from K1[0]
		K1[0] = K1[0]<<1;
	}
	else {
		K1[1] = ( (L[0] & 0x40000000) == 0x40000000 ) ? ( (L[1]<<1 | 0x00000001) ^ Rb_HI ) : ( (L[1]<<1) ^ Rb_HI );
		K1[0] = (L[0]<<1) ^ Rb_LO;
	}

	// (Disable K2 generation since it's not being used)
	//// Step 3: K2 generation
	//if ( (K1[1] & 0x40000000) == 0x00000000 ) {
	//	K2[1] = ( (K1[0] & 0x40000000) == 0x40000000 ) ? ( K1[1]<<1 | 0x00000001 ) : ( K1[1]<<1 ); // shift left and add 1 carry to K2[1] from K2[0]
	//	K2[0] = K1[0]<<1;
	//}
	//else {
	//	K2[1] = ( (K1[0] & 0x40000000) == 0x40000000 ) ? ( (K1[1]<<1 | 0x00000001) ^ Rb_HI ) : ( (K1[1]<<1) ^ Rb_HI );
	//	K2[0] = (K1[0]<<1) ^ Rb_LO;
	//}
}
#endif


// CMAC algorithm based on MISTY1 block cipher; We choose length(T) == MISTY1_BLOCKSIZE (in bytes)
// Note: In this instance, K2 is not used as M_len is a multiple of the MISTY1_BLOCKSIZE; i.e. computations are simplified
// IMPORTANT: M must be a multiple of 8 bytes (i.e. 2*u4); T must be preallocated as u4 T[2];
int cmac(u4 *ek, u4 M[], int M_len, u4 *T, u4 *K1/*, u4 *K2*/) {
	u4 C[2] = {ZERO_LO, ZERO_HI};
	int i;

	if ( (M_len<2) && (!noskip_flag) )
		return -1;

	// (pre)mask final message block with K1 since the block is exactly equal to the MISTY1_BLOCKSIZE
	M[M_len-2] ^= K1[0];
	M[M_len-1] ^= K1[1];

	for (i=0; i<M_len; i+=2) {
		C[0] ^= M[i];
		C[1] ^= M[i+1];
		misty1_encrypt_block(ek, C, C);
	}
	T[0] = C[0];
	T[1] = C[1];

	return 0;
}


// =================================================
// == Main function ================================
// =================================================

int main(void) {
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ++ Shared variables ++
	u4 RF_IN[RF_IN_LEN];
	u4 RF_OUT[RF_OUT_LEN];

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ++ IMPLANT-side variables ++
	u4  i_key[MISTY1_KEYSIZE/4] = {KEY_1, KEY_2, KEY_3, KEY_4}; // shared private key
	u4 i_ek_e[MISTY1_EXPKEYSIZE]; // expanded private key for encryption
	u4 i_ek_d[MISTY1_EXPKEYSIZE]; // expanded private key for decryption

	u4 i_k1[2]; // K1 block-cipher subkey
	//u4 i_k2[2]; // K2 block-cipher subkey

	u4 Ni;
	u4 Tij_prev[2] = {SEEDi_LO, SEEDi_HI}; // random seed provided by the factory
	u4 Tij_curr[2];

	u4 i_Nr; // Copy of Nr in the IMPLANT

	u4 i_cCMD[2]; // variable to store encrypted READER command
	u4 i_pCMD[2]; // variable to store decrypted READER command

	u4 pANS[ANSi_LEN];
	u4 cANS[ANSi_LEN];
	int ANS_size = 0; // actual length of transmitted answer; must be a multiple of 2*u4; in the range [0..ANSi_LEN]

	u4 Mi[MACi_INLEN];
	u4 MACi[2];

	u4 i_Mr[MACr_INLEN];
	u4 i_MACr[2];

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ++ READER-side variables ++
	#if READEREMU==1
	noskip_flag = 0;

	u4  r_key[MISTY1_KEYSIZE/4] = {KEY_1, KEY_2, KEY_3, KEY_4}; // shared private key
	u4 r_ek_e[MISTY1_EXPKEYSIZE]; // expanded private key for encryption
	u4 r_ek_d[MISTY1_EXPKEYSIZE]; // expanded private key for decryption

	u4 r_k1[2]; // K1 block-cipher subkey
	//u4 r_k2[2]; // K2 block-cipher subkey

	u4 Nr;
	u4 Trj_prev[2] = {SEEDr_LO, SEEDr_HI}; // random seed provided by the factory
	u4 Trj_curr[2];

	u4 r_Ni; // Copy of Ni in the READER

	u4 pCMD[2]; // CMD high word is used for padding
	u4 cCMDr[2];

	u4 Mr[MACr_INLEN];
	u4 MACr[2];

	u4 r_cANS[ANSi_LEN]; // variable to store decrypted IMPLANT answer
	u4 r_pANS[ANSi_LEN]; // variable to store decrypted IMPLANT answer
	int r_ANS_size = 0; // actual length of received answer; must be a multiple of 2*u4; in the range [0..ANSi_LEN]

	u4 r_Mi[MACi_INLEN];
	u4 r_MACi[2];
	#endif

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ++ Common variables ++
	int i;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// AT THIS POINT, THE SISC (AND TRANSCEIVER) IS RUNNING ON POWER SCAVENGED FROM THE RF-LINK
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	#if KEYGEN==1
	// Generating expanded keys in IMPLANT
	#if PRINTOUTS==1
	printf("\n\tGenerating expanded keys and subkeys in IMPLANT.");
	#endif

	misty1_keyinit(i_ek_e, i_key);
	misty1_keyinit(i_ek_d, i_key);

	if (memcmp(i_ek_e, i_ek_d, MISTY1_KEYSIZE*sizeof(u4))) {
		#if PRINTOUTS==1
		printf("\n\tInternal IMPLANT Error: keysch is wrong.");
		#endif
		return -1;
	}

	// Generating subkeys in IMPLANT
	subkeys(i_ek_e, i_k1/*, i_k2*/);

	#if READEREMU==1
		// Generating expanded keys in READER
		#if PRINTOUTS==1
		printf("\n\tGenerating expanded keys and subkeys in READER.");
		#endif

		misty1_keyinit(r_ek_e, r_key);
		misty1_keyinit(r_ek_d, r_key);

		if (memcmp(r_ek_e, r_ek_d, MISTY1_KEYSIZE*sizeof(u4))) {
			#if PRINTOUTS==1
			printf("\n\tInternal READER Error: keysch is wrong.");
			#endif
			return -2;
		}

		// Generating subkeys in READER
		subkeys(r_ek_e, r_k1/*, r_k2*/);
	#endif


	#elif KEYGEN==0
	// Precalculated expanded keys in IMPLANT
	i_ek_e[0] = 0x00000011; i_ek_e[1] = 0x00002233; i_ek_e[2] = 0x00004455; i_ek_e[3] = 0x00006677; i_ek_e[4] = 0x00008899; 
	i_ek_e[5] = 0x0000AABB; i_ek_e[6] = 0x0000CCDD; i_ek_e[7] = 0x0000EEFF; i_ek_e[8] = 0x0000CF51; i_ek_e[9] = 0x00008E7F; 
	i_ek_e[10] = 0x00005E29; i_ek_e[11] = 0x0000673A; i_ek_e[12] = 0x0000CDBC; i_ek_e[13] = 0x000007D6; i_ek_e[14] = 0x0000BF35; 
	i_ek_e[15] = 0x00005E11; i_ek_e[16] = 0x00000151; i_ek_e[17] = 0x0000007F; i_ek_e[18] = 0x00000029; i_ek_e[19] = 0x0000013A; 
	i_ek_e[20] = 0x000001BC; i_ek_e[21] = 0x000001D6; i_ek_e[22] = 0x00000135; i_ek_e[23] = 0x00000011; i_ek_e[24] = 0x00000067; 
	i_ek_e[25] = 0x00000047; i_ek_e[26] = 0x0000002F; i_ek_e[27] = 0x00000033; i_ek_e[28] = 0x00000066; i_ek_e[29] = 0x00000003; 
	i_ek_e[30] = 0x0000005F; i_ek_e[31] = 0x0000002F;

	for (i=0; i<MISTY1_EXPKEYSIZE; i++)
		i_ek_d[i] = i_ek_e[i];

	// Precalculated subkeys in IMPLANT
	i_k1[0] = 0x0C3B1EFB; i_k1[1] = 0xD129B354;
	//i_k2[0] = 0x18763DED; i_k2[1] = 0xA25366A8;

	#if READEREMU==1
		// Precalculated expanded keys in READER
		r_ek_e[0] = 0x00000011; r_ek_e[1] = 0x00002233; r_ek_e[2] = 0x00004455; r_ek_e[3] = 0x00006677; r_ek_e[4] = 0x00008899; 
		r_ek_e[5] = 0x0000AABB; r_ek_e[6] = 0x0000CCDD; r_ek_e[7] = 0x0000EEFF; r_ek_e[8] = 0x0000CF51; r_ek_e[9] = 0x00008E7F; 
		r_ek_e[10] = 0x00005E29; r_ek_e[11] = 0x0000673A; r_ek_e[12] = 0x0000CDBC; r_ek_e[13] = 0x000007D6; r_ek_e[14] = 0x0000BF35; 
		r_ek_e[15] = 0x00005E11; r_ek_e[16] = 0x00000151; r_ek_e[17] = 0x0000007F; r_ek_e[18] = 0x00000029; r_ek_e[19] = 0x0000013A; 
		r_ek_e[20] = 0x000001BC; r_ek_e[21] = 0x000001D6; r_ek_e[22] = 0x00000135; r_ek_e[23] = 0x00000011; r_ek_e[24] = 0x00000067; 
		r_ek_e[25] = 0x00000047; r_ek_e[26] = 0x0000002F; r_ek_e[27] = 0x00000033; r_ek_e[28] = 0x00000066; r_ek_e[29] = 0x00000003; 
		r_ek_e[30] = 0x0000005F; r_ek_e[31] = 0x0000002F; 

		for (i=0; i<MISTY1_EXPKEYSIZE; i++)
			r_ek_d[i] = r_ek_e[i];

		// Precalculated subkeys in READER
		r_k1[0] = 0x0C3B1EFB; r_k1[1] = 0xD129B354;
		//r_k2[0] = 0x18763DED; r_k2[1] = 0xA25366A8;
	#endif
	#endif

	//printf("\nMAIN:");
	//for (i=0; i<MISTY1_EXPKEYSIZE; i++)
	//	printf("\ni_ek_e[%d] = 0x%08X; ", i, i_ek_e[i]);
	//printf("\n");


	///////////////////////////////////////////////////////////////////////////////////////
	//-- (1) (READER SIDE) R => IMD: "hello"
	///////////////////////////////////////////////////////////////////////////////////////
	#if READEREMU==1
		RF_IN[0] = 0x68656C6C; // "hell
		RF_IN[1] = 0x6F000000; //  o"
		RF_IN[2] = 0x00000000;
		RF_IN[3] = 0x00000000;
		RF_IN[4] = 0x00000000;
		RF_IN[5] = 0x00000000;
		#if PRINTOUTS==1 && MSGDUMP==1
		printf("\n- RF_IN:");
		printm(RF_IN, RF_IN_LEN);
		#endif

		#if PRINTOUTS==1
		printf("\n\n(1) R => IMD: hello\n");
		#endif
	#endif


	///////////////////////////////////////////////////////////////////////////////////////
	//-- (2) (IMPLANT SIDE) IMD => R: "Ni"
	///////////////////////////////////////////////////////////////////////////////////////
        // Checking if a 'hello' signal has been sent
	if ( ( (RF_IN[0] != 0x68656C6C) || (RF_IN[1] != 0x6F000000) ||
	       (RF_IN[2] != 0x00000000) || (RF_IN[3] != 0x00000000) ||
	       (RF_IN[4] != 0x00000000) || (RF_IN[5] != 0x00000000) ) && (!noskip_flag) ) {
		#if PRINTOUTS==1
		printf("\n\tNo 'hello' message received. Powering down SISC.\n\n");
		#endif
		return 0;
	}
	#if PRINTOUTS==1
	else {
		printf("\n\tImplant received 'hello' message from READER.");
	}
	#endif

	// Generating new IMPLANT random number
	#if PRINTOUTS==1
	printf("\n\tGenerating new IMPLANT random number.");
	#endif
	Ni = crand(i_ek_e, Tij_prev, Tij_curr);
	Tij_prev[1] = Tij_curr[1];
	Tij_prev[0] = Tij_curr[0];


	// Sending new nonse to READER
	#if PRINTOUTS==1
	printf("\n\tSending new Ni to READER (%08X).", Ni);
	#endif
	RF_OUT[0] = Ni;
	RF_OUT[1] = 0x00000000;
	RF_OUT[2] = 0x00000000;
	RF_OUT[3] = 0x00000000;
	RF_OUT[4] = 0x00000000;
	RF_OUT[5] = 0x00000000;

	#if PRINTOUTS==1 && MSGDUMP==1
	printf("\n- RF_OUT (I: Ni):");
	printm(RF_OUT, 6);
	#endif

	#if PRINTOUTS==1
	printf("\n\n(2) IMD => R: Ni\n");
	#endif

	///////////////////////////////////////////////////////////////////////////////////////
	//-- (3) (READER SIDE) R => IMD: "Nr, Ni, {Nr, Ni, IDi, CMD)}_Kri, |CMD|_Kri"
	///////////////////////////////////////////////////////////////////////////////////////
	#if READEREMU==1
		// Generating new READER random number
		#if PRINTOUTS==1
		printf("\n\tGenerating new READER random number.");
		#endif
		Nr = crand(r_ek_e, Trj_prev, Trj_curr);
		Trj_prev[1] = Trj_curr[1];
		Trj_prev[0] = Trj_curr[0];

		// Storing new IMPLANT random number
		#if PRINTOUTS==1
		printf("\n\tStoring new IMPLANT random number (%08X).", RF_OUT[0]);
		#endif
		r_Ni = RF_OUT[0];

		// Generating new READER CMD
		#if PRINTOUTS==1
		printf("\n\tGenerating new READER CMD.");
		#endif
		//encode(pCMD[0]); // FIXME
		pCMD[0] = 0xEF012345; // dummy command
		pCMD[1] = 0x00000000; // CMD high word is used for padding
		r_ANS_size = 4; // the READER a priori knows the size of the expected IMPLANT answer (needed for proper data reception)

		// Generating READER MAC
		#if PRINTOUTS==1
		printf("\n\tGenerating READER MAC.");
		#endif

		Mr[0] = Nr;
		Mr[1] = r_Ni;
		Mr[2] = IDi_HI;
		Mr[3] = IDi_ME;
		Mr[4] = IDi_LO;
		Mr[5] = pCMD[0];

		if ( (cmac(r_ek_e, Mr, MACr_INLEN, MACr, r_k1/*, r_k2*/)) && (!noskip_flag) ) {
			#if PRINTOUTS==1
			printf("\n\tCannot calculate MAC for READER messages smaller than the cipher block size.");
			#endif
			return -3;
		}

		// Generating READER encrypted CMD
		#if PRINTOUTS==1
		printf("\n\tGenerating READER encrypted CMD.");
		#endif
		misty1_encrypt_block(r_ek_e, pCMD, cCMDr);

		// Sending MAC+CMD to IMPLANT
		#if PRINTOUTS==1
		printf("\n\tSending new MAC+CMD to IMPLANT (%08X %08X + |%08X|).", MACr[1], MACr[0], pCMD[0]);
		#endif
		RF_IN[0] = Nr;
		RF_IN[1] = r_Ni;
		RF_IN[2] = MACr[1]; // MAC high
		RF_IN[3] = MACr[0]; // MAC low
		RF_IN[4] = cCMDr[1]; // cCMD high
		RF_IN[5] = cCMDr[0]; // cCMD low

		#if PRINTOUTS==1 && MSGDUMP==1
		printf("\n- RF_IN (R: MAC+CMD):");
		printm(RF_IN, RF_IN_LEN);
		#endif

		#if PRINTOUTS==1
		printf("\n\n(3) R => IMD: Nr, Ni, {Nr, Ni, IDi, CMD}_Kri, |CMD|_Kri\n");
		#endif
	#endif


	///////////////////////////////////////////////////////////////////////////////////////
	//-- (4) (IMPLANT SIDE) IMD => R: "{Ni, Nr, ANS}_Kri, |ANS|_Kri"
	///////////////////////////////////////////////////////////////////////////////////////
	// Storing new READER random number
	#if PRINTOUTS==1
	printf("\n\tStoring new READER random number.");
	#endif
	i_Nr = RF_IN[0];

	// IMPLANT decrypting CMD
	#if PRINTOUTS==1
	printf("\n\tIMPLANT decrypting CMD.");
	#endif
	i_cCMD[1] = RF_IN[4];
	i_cCMD[0] = RF_IN[5];
	misty1_decrypt_block(i_ek_d, i_cCMD, i_pCMD);

	// IMPLANT performing checks
	#if PRINTOUTS==1
	printf("\n\tIMPLANT performing checks.");
	#endif
	if ( (RF_IN[1] != Ni)  && (!noskip_flag) ) {
		#if PRINTOUTS==1
		printf("\n\tNi received from READER is incorrect. Powering down SISC.\n\n");
		#endif
		return 0;
	}
	#if PRINTOUTS==1
	else {
		printf("\n\tNi received from READER is correct.");
	}
	#endif

	i_Mr[0] = i_Nr;
	i_Mr[1] = Ni;
	i_Mr[2] = IDi_HI;
	i_Mr[3] = IDi_ME;
	i_Mr[4] = IDi_LO;
	i_Mr[5] = i_pCMD[0];

	if ( (cmac(i_ek_e, i_Mr, MACr_INLEN, i_MACr, i_k1/*, r_k2*/)) && (!noskip_flag) ) {
			#if PRINTOUTS==1
			printf("\n\tCannot verify MAC for READER messages smaller than the cipher block size.");
			#endif
			return -4;
	}

	if ( ((i_MACr[0] != RF_IN[3]) || (i_MACr[1] != RF_IN[2])) && (!noskip_flag) ) {
		#if PRINTOUTS==1
		printf("\n\tMAC received from READER is corrupt. Powering down SISC.\n\n");
		#endif
		return 0;
	}
	#if PRINTOUTS==1
	else {
		printf("\n\tMAC received from READER is correct. ==> READER has been authenticated!");
		printf("\n========> SWITCHING TO BATTERY POWER! ========>");
	}
	#endif

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// AT THIS POINT, THE SISC CAN SWITCH TO BATTERY POWER IN ORDER TO TRANSMIT 'LARGE' DATA CHUNKS
// (OR, IT COULD SWITCH DEPENDING ON THE 'i_pCMD' RECEIVED: ON A FEW DATA, THE RF LINK MIGHT STILL BE USED.
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	// Some decoding tree is needed here:
	// FIXME (add some more elaborate decoding routine)
	// u4 ANS = decode(CMD); // 
	ANS_size = 4; // MUST be a multiple of 2*u4; max is ANSi_LEN
	pANS[0] = 0xFFFA0001; // dummy answer
	pANS[1] = 0xFFFB0002; // dummy answer
	pANS[2] = 0xFFFC0003; // dummy answer
	pANS[3] = 0xFFFD0004; // dummy answer

	// Generating IMPLANT MAC
	#if PRINTOUTS==1
	printf("\n\tGenerating IMPLANT MAC.");
	#endif

	Mi[0] = Ni;
	Mi[1] = i_Nr;
	for (i=0; i<ANS_size; i++)
		Mi[2+i] = pANS[i];

	if ( (cmac(i_ek_e, Mi, (1 + 1 + ANS_size), MACi, i_k1/*, r_k2*/)) && (!noskip_flag) ) {
			#if PRINTOUTS==1
			printf("\n\tCannot calculate MAC for IMPLANT messages smaller than the cipher block size.");
			#endif
			return -5;
	}

	// Generating IMPLANT encrypted ANS
	#if PRINTOUTS==1
	printf("\n\tGenerating IMPLANT encrypted ANS.");
/*, r_k2*/	#endif
	for (i=0; i<ANS_size; i+=2)
		misty1_encrypt_block(i_ek_e, &pANS[i], &cANS[i]);

	// Sending new MAC+ANS to READER
	#if PRINTOUTS==1
	printf("\n\tSending new MAC+ANS to READER (%08X %08X + |%08X %08X ..|).", MACi[1], MACi[0], pANS[0], pANS[1]);
	#endif
	RF_OUT[0] = MACi[1]; // MAC high
	RF_OUT[1] = MACi[0]; // MAC low
	for (i=0; i<ANS_size; i++)
		RF_OUT[2+i] = cANS[i];

	#if PRINTOUTS==1 && MSGDUMP==1
	printf("\n- RF_OUT (I: MAC+ANS):");
	printm(RF_OUT, (2 + ANS_size));
	#endif

	#if PRINTOUTS==1
	printf("\n\n(4) IMD => R: {Ni, Nr, ANS}_Kri, |ANS|_Kri\n");
	#endif


	///////////////////////////////////////////////////////////////////////////////////////
	//-- (5) (READER SIDE) R: Authenticating!
	///////////////////////////////////////////////////////////////////////////////////////
	#if READEREMU==1
		// READER decrypting ANS
		#if PRINTOUTS==1
		printf("\n\tREADER decrypting ANS.");
		#endif
		for (i=0; i<r_ANS_size; i+=2) {
			r_cANS[i]   = RF_OUT[2+i];
			r_cANS[i+1] = RF_OUT[2+i+1];
			misty1_decrypt_block(r_ek_d, &r_cANS[i], &r_pANS[i]);
		}

		#if PRINTOUTS==1 && MSGDUMP==1
		printf("\n- RF_OUT (R: ANS):");
		printm(r_pANS, ANS_size);
		#endif

		// READER performing checks
		#if PRINTOUTS==1
		printf("\n\tREADER performing checks.");
		#endif

		r_Mi[0] = r_Ni;
		r_Mi[1] = Nr;
		for (i=0; i<r_ANS_size; i++)
			r_Mi[2+i] = r_pANS[i];

		if ( (cmac(r_ek_e, r_Mi, (1 + 1 + r_ANS_size), r_MACi, r_k1/*, r_k2*/)) && (!noskip_flag) ) {
			#if PRINTOUTS==1
			printf("\n\tCannot verify MAC for IMPLANT messages smaller than the cipher block size.");
			#endif
			return -6;
		}

		if ( (r_MACi[0] != RF_OUT[1]) || (r_MACi[1] != RF_OUT[0]) ) {
			#if PRINTOUTS==1
			printf("\n\tMAC received from IMPLANT is corrupt. Powering down SISC.\n\n");
			#endif
			return 0;
		}
		#if PRINTOUTS==1
		else {
			printf("\n\tMAC received from READER is correct. ==> IMPLANT has been authenticated!");
		}
		#endif

		// DO SOMETHING WITH THIS ANSWER....
		// ...
		// ...

		#if PRINTOUTS==1
		printf("\n\tDestroying expanded keys in READER!");
		#endif
		misty1_key_destroy(r_ek_e);
		misty1_key_destroy(r_ek_d);
	#endif


	#if PRINTOUTS==1
	printf("\n\tDestroying expanded keys in IMPLANT!");
	#endif
	misty1_key_destroy(i_ek_e);
	misty1_key_destroy(i_ek_d);

	#if PRINTOUTS==1
	printf("\n\nFinished simulation successfully!\n\n");
	#endif

        return 0;
}

/*, r_k2*/
