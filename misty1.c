/*
 *
 * Author: Hironobu SUZUKI
 * Copyright Notice: Copyright (C) 1998, Hironobu SUZUKI.
 * Copyright Condition: GNU GENERAL PUBLIC LICENSE Version 2
 * Date: 11 January 1998
 *
 * AND:
 * Modified Version for Implantable Medical Device Application
 * Copyright (c) 2012, Neurasmus B.V., The Netherlands,
 * web: www.neurasmus.com email: info@neurasmus.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *

 * Author: Christos Strydis
 * Created: 01-07-2012
 * Modified: 17-07-2012
 *
 */


//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
#include "misty1.h"


u4 fi( u4  fi_in, u4  fi_key) {
	u4  d9, d7;

	d9 = (fi_in >> 7) & 0x1ff;
	d7 = fi_in & 0x7f;
	d9 = s9[d9] ^ d7;
	d7 = (s7[d7] ^ d9) & 0x7f;

	d7 = d7 ^ ((fi_key >> 9) & 0x7f);
	d9 = d9 ^ (fi_key & 0x1ff);
	d9 = s9[d9] ^ d7;
	return ((d7<<9) | d9);
}


u4  fo(u4  *ek, u4  fo_in, byte k) {
	u4 t0, t1;
	t0 = (fo_in >> 16);
	t1 = fo_in & 0xFFFF;
	t0 ^= ek[k];
	t0 = fi(t0, ek[((k+5)%8) + 8]);
	t0 ^= t1;
	t1 ^= ek[(k+2)%8];
	t1 = fi(t1,ek[((k+1)%8) + 8]);
	t1 ^= t0;
	t0 ^= ek[(k+7)%8];
	t0 = fi(t0, ek[((k+3)%8)+8]);
	t0 ^= t1;
	t1 ^=  ek[(k+4)%8];
	return ( (t1<<16) | t0 );
}


u4  fl(u4  *ek, u4  fl_in, byte k) {
	u4  d0, d1;
	byte t;

	d0 = (fl_in >> 16); 
	d1 = fl_in & 0xffff;
	if ( k % 2 ) {
		t = (k-1)/2;
		d1 = d1 ^ (d0 & ek[((t+2)%8)+8]);
		d0 = d0 ^ (d1 | ek[(t+4)%8]);
	}
	else {
		t = k/2;
		d1 = d1 ^ (d0 & ek[t]);
		d0 = d0 ^ (d1 | ek[((t+6)%8)+8]);
	}
	return ((d0<<16) | d1);
}


u4  flinv(u4  *ek, u4  fl_in, byte k) {
	u4  d0, d1;
	byte t;

	d0 = (fl_in >> 16);
	d1 = fl_in & 0xffff;

	if ( k % 2 ) {
		t = (k-1)/2;
		d0 = d0 ^ (d1 | ek[(t+4)%8]);
		d1 = d1 ^ (d0 & ek[((t+2)%8)+8]);
	}
	else {
		t = k/2;
		d0 = d0 ^ (d1 | ek[((t+6)%8)+8]);
		d1 = d1 ^ (d0 & ek[t]);
	}
	return ((d0<<16) | d1);
}


void misty1_encrypt_block(u4 *ek, u4 p[2], u4 c[2]) {
	u4  d0, d1;

	d0 = p[0];
	d1 = p[1];

	/* 0 round */
	d0 = fl(ek,d0,0);
	d1 = fl(ek,d1,1);
	d1 = d1 ^fo(ek,d0,0);

	/* 1 */
	d0 = d0^fo(ek,d1,1);

	/* 2 */
	d0 = fl(ek,d0,2);
	d1 = fl(ek,d1,3);
	d1 = d1 ^fo(ek,d0,2);

	/* 3 */
	d0 = d0^fo(ek,d1,3);

	/* 4 */
	d0 = fl(ek,d0,4);
	d1 = fl(ek,d1,5);
	d1 = d1 ^fo(ek,d0,4);


	/* 5 */
	d0 = d0^fo(ek,d1,5);

	/* 6 */
	d0 = fl(ek,d0,6);
	d1 = fl(ek,d1,7);
	d1 = d1 ^fo(ek,d0,6);


	/* 7 */
	d0 = d0^fo(ek,d1,7);

	/* 8 */
	d0 = fl(ek,d0,8);
	d1=fl(ek,d1,9);

	/* round finished */
	c[0] = d1;
	c[1] = d0;
}


void misty1_decrypt_block(u4 *ek, u4 c[2], u4 p[2]) {
	u4  d0, d1;

	d0 = c[1];
	d1 = c[0];
	d0 = flinv(ek,d0, 8);
	d1 = flinv(ek,d1, 9);
	d0 = d0 ^ fo(ek,d1, 7);
	d1 = d1 ^ fo(ek,d0, 6);
	d0 = flinv(ek,d0, 6);
	d1 = flinv(ek,d1, 7);
	d0 = d0 ^ fo(ek,d1, 5);
	d1 = d1 ^ fo(ek,d0, 4);
	d0 = flinv(ek,d0, 4);
	d1 = flinv(ek,d1, 5);
	d0 = d0 ^ fo(ek,d1, 3);
	d1 = d1 ^ fo(ek,d0, 2);
	d0 = flinv(ek,d0, 2);
	d1 = flinv(ek,d1, 3);
	d0 = d0 ^ fo(ek,d1, 1);
	d1 = d1 ^ fo(ek,d0, 0);
	d0 = flinv(ek,d0, 0);
	d1 = flinv(ek,d1, 1);
	p[0] = d0;
	p[1] = d1;
}


void bcopy_u4_byte(u4 k, byte * b) { 
	b[0] = (k>>24)&0xFF;
	b[1] = (k>>16)&0xFF;
	b[2] = (k>>8)&0xFF;
	b[3] =  k &0xFF;
}


void misty1_keyinit(u4  *ek, u4  *k) {
	int i;
	byte key[16];

	bcopy_u4_byte(k[0],(byte *)(&key[0]));
	bcopy_u4_byte(k[1],(byte *)(&key[4]));
	bcopy_u4_byte(k[2],(byte *)(&key[8]));
	bcopy_u4_byte(k[3],(byte *)(&key[12]));

	//memset(ek,0x00,MISTY1_KEYSIZE*sizeof(u4));
	my_memset (ek, 0x00, MISTY1_KEYSIZE*sizeof(u4));
	for(i=0; i < 8 ; i++) {
		ek[i] = (key[i*2]*256) + (key[(i*2) +1]);
	}

	for (i=0 ; i < 8 ; i++) {
		ek[i+8]  = fi(ek[i],ek[(i+1)%8]);
		ek[i+16] = ek[i+8] & 0x1ff;
		ek[i+24] = ek[i+8] >> 9;
	}
}


void misty1_key_destroy(u4  *ek) {
	//memset(ek,0x00,MISTY1_KEYSIZE*sizeof(u4));
	my_memset (ek, 0x00, MISTY1_KEYSIZE*sizeof(u4));
}


/*
 * Symmetric encrypt function
 * (InLen : Input size in Bytes! - OutLen: Output size in Bytes; might be differentfrom InLen due to padding!)
 */
/*
int Encrypt(u4 * InBlocks, long long InLen, byte InKey[MISTY1_KEYSIZE], u4 * OutBlocks, long long * OutLen) {
	long long InCount = 0;  // count in multiples of u4!
	long long OutCount = 0; // count in multiples of u4!
	
	// set key to MISTY1 key size
	int keysize = MISTY1_KEYSIZE;

	// allocate memory for expanded key
	u4 * key; // key size: 16 bytes
	//u4  key[]= {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
	key = (u4 *) malloc((keysize<<2)*sizeof(byte));
	memcpy(key, InKey, keysize);

	u4 ek_e[MISTY1_EXPKEYSIZE],  ek_d[MISTY1_EXPKEYSIZE];
	misty1_keyinit(ek_e, key);
	misty1_keyinit(ek_d, key);

	if (memcmp(ek_e, ek_d, MISTY1_EXPKEYSIZE*sizeof(u4))) {
		printf("Internal Error: keysch is wrong.\n");
		return -1;
	}
	
	if (InLen <=0) {
		printf("Input size is less or equal to zero.\n");
		return -2;
	}

	// start iterations on plaintext
	while (InLen > 0) {
		u4 datain[2] = {0, 0}; // block size: 8 bytes
		//u4  data[]= {0x01234567, 0x89abcdef};
		InLen -= MISTY1_BLOCKSIZE; // block size is static (8 Bytes)
		
		// If the plaintext is not a multiple of MISTY1_BLOCKSIZE, output data will be padded
		if (InLen < 0) {
			memcpy((void *)&datain[0], (void *)&InBlocks[InCount], (MISTY1_BLOCKSIZE+InLen));
			InCount += 2;
		}
		else {
			for (int i=0; i<(MISTY1_BLOCKSIZE/sizeof(u4)); i++)
				datain[i] = InBlocks[InCount++];
		}
		
		// encrypt
		u4 dataout[2] = {0, 0}; // block size: 8 byte
		misty1_encrypt_block(ek_e, datain, dataout);

		OutBlocks[OutCount++] = dataout[0];
		OutBlocks[OutCount++] = dataout[1];
	} //while
	
	*OutLen = OutCount * sizeof(u4); // count actual characters

	misty1_key_destroy(ek_e);
	misty1_key_destroy(ek_d);
	memset(key, 0x00, (keysize<<2)*sizeof(byte));
	
	free(key);
	
	return 0;
}
*/


/*
 * Symmetric decrypt function
 * (InLen : Input size in Bytes! - OutLen: Output size in Bytes; might be differentfrom InLen due to padding!)
 */
/*
int Decrypt(u4 * InBlocks, long long InLen, byte InKey[MISTY1_KEYSIZE], u4 * OutBlocks, long long * OutLen) {
	long long InCount = 0;  // count in multiples of u4!
	long long OutCount = 0; // count in multiples of u4!
	
	// set key to MISTY1 key size
	int keysize = MISTY1_KEYSIZE;

	// allocate memory for expanded key
	u4 * key; // key size: 16 bytes
	//u4  key[]= {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
	key = (u4 *) malloc((keysize<<2)*sizeof(byte));
	memcpy(key, InKey, keysize);

	u4 ek_e[MISTY1_EXPKEYSIZE],  ek_d[MISTY1_EXPKEYSIZE];
	misty1_keyinit(ek_e, key);
	misty1_keyinit(ek_d, key);

	if (memcmp(ek_e, ek_d, MISTY1_EXPKEYSIZE*sizeof(u4))) {
		printf("Internal Error: keysch is wrong\n");
		return -1;
	}
	
	if (InLen <=0) {
		printf("Input size is less or equal to zero.\n");
		return -2;
	}

	// start iterations on plaintext
	while (InLen > 0) {
		u4 datain[2] = {0, 0}; // block size: 8 bytes
		//u4  data[]= {0x01234567, 0x89abcdef};
		InLen -= MISTY1_BLOCKSIZE; // block size is static (8 Bytes)

		// If the plaintext is not a multiple of MISTY1_BLOCKSIZE, output data will be padded
		if (InLen < 0) {
			memcpy((void *)&datain[0], (void *)&InBlocks[InCount], (MISTY1_BLOCKSIZE+InLen));
			InCount += 2;
		}
		else {
			for (int i=0; i<(MISTY1_BLOCKSIZE/sizeof(u4)); i++)
				datain[i] = InBlocks[InCount++];
		}
		
		// decrypt
		u4 dataout[2] = {0, 0}; // block size: 8 byte
		misty1_decrypt_block(ek_e, datain, dataout);
		
		OutBlocks[OutCount++] = dataout[0];
		OutBlocks[OutCount++] = dataout[1];
	} //while

	*OutLen = OutCount * sizeof(u4); // count actual characters

	misty1_key_destroy(ek_e);
	misty1_key_destroy(ek_d);
	memset(key, 0x00, (keysize<<2)*sizeof(byte));
	
	free(key);
	
	return 0;
}
*/

void my_memset (void * buff, int val, int num) {
	int *b = (int*) buff;
	int i = 0;

	for(i=0; i<num; i++) {
	     *b = val;
	     b++;
	}
}


